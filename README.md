# A.E.S. - resources list

Custom Content for A.E.S. 
Everything here goes in the world folder.

For people updating the resources list: Make changes to the repo, then git pull in the world folder. The repository is hiding inside the world folder on both the test server and the live server. 

Any textures for server events or custom npcs should go in _world_folder_media

Make sure to update licenses when you make changes.
