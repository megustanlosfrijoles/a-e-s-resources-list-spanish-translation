# „Videomaker“-Chat-Präfix!

Machen Sie **Videos** auf YouTube/Peertube? Lassen Sie es alle wissen, und lösen Sie Ihr „Videomaker“-Chat-Präfix ein!
Für weitere Informationen treten Sie unserem Matrix-Raum ```#minetest-aes:matrix.org``` bei.
